% use paper, or submit
% use 10pt, 11pt or 12pt
% nopagenum removes the page numbers (final conference PDF paper)

%\documentclass[preprint, paper,11pt]{AAS}	% for preprint proceedings
%\documentclass[]{aiaa-tc}		% for final proceedings (20-page limit)
\documentclass[paper,12pt]{AAS}		% for final proceedings (20-page limit)
%\documentclass[paper,10pt]{AAS}		% for final proceedings (20-page limit)
%\documentclass[cover, article,11pt]{AAS}		% for journal paper version
%\documentclass[submit]{AAS}					% to submit to JAS

\usepackage{bm}
\usepackage{amsmath}
%\usepackage[notref,notcite]{showkeys}  % use this to temporarily show labels
\usepackage[colorlinks=true, pdfstartview=FitV, linkcolor= black, citecolor= black, urlcolor= black]{hyperref}
\usepackage{overcite}
\usepackage{AVS}
\usepackage{graphicx}
\usepackage{float}
\usepackage{wrapfig}
\usepackage{varioref}%  smart page, figure, table, and equation referencing
\usepackage{wrapfig}%   wrap figures/tables in text (i.e., Di Vinci style)
%\usepackage{threeparttable}% tables with footnotes
\usepackage{dcolumn}%   decimal-aligned tabular math columns
\newcolumntype{d}{D{.}{.}{-1}}
\usepackage{nomencl}%   nomenclature generation via makeindex
\makeglossary
%\usepackage{subfigure}% subcaptions for subfigures
\usepackage{subfig}
%\usepackage{subfigmat}% matrices of similar subfigures, aka small mulitples
\usepackage{fancyvrb}%  extended verbatim environments
\fvset{fontsize=\footnotesize,xleftmargin=2em}
\usepackage{lettrine}%  dropped capital letter at beginning of paragraph

\usepackage{color}
\usepackage[normalem]{ulem} % added this to enable sout for strike outs

\definecolor{RED}{rgb}{1,0,0} 
\definecolor{BLUE}{rgb}{0,0,1} 
\definecolor{GREEN}{rgb}{0,0,1} 
%\newcommand{\EditHPS}[1]{{\color{red} #1}}
%\newcommand{\EditHPSd}[1]{{\color{red} \sout{ #1}}}
\newcommand{\EditEH}[1]{{\color{blue} #1}}
\newcommand{\EditEHd}[1]{{\color{blue} \sout{ #1}}}

% Define commands to assure consistent treatment throughout document
\newcommand{\eqnref}[1]{(\ref{#1})}
\newcommand{\class}[1]{\texttt{#1}}
\newcommand{\package}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{#1}}
\newcommand{\BibTeX}{\textsc{Bib}\TeX}

\graphicspath{{./figures/} }
% for a journal simulation cover page:

%\JournalName{Journal of Guidance, Navigation and Control}
%\JournalIssue{Volume~xx, Number~xx, Jan.--Feb., 2001, Pages xx--xx}

% journal article simulation:

%\ArticleIssue{Vol.~24, No.~1, Jan.--Feb., 2001}% first page
%\ArticleHeader{Schaub Et Al: New Penalty Functions}% subsequent pages

% journal note simulation:

%\NoteHeader{J.Guidance, Vol.~20, No.~13: Engineering Notes}

% set copyright and other notices to appear
% as a footnote at the bottom of the first page:

%\PaperNotice{\CopyrightB{1998}{Hanspeter Schaub}}

\title{Spacecraft Decision-Making Autonomy using Deep Reinforcement Learning}

\author{Andrew Harris,\thanks{Research Assistant, Ann and H.J. Smead Department of Aerospace Engineering Sciences, University of Colorado Boulder, Boulder,
		CO, 80309 USA.}\ 
	Thibaud Teil,\thanks{Research Assistant, Ann and H.J. Smead Department of Aerospace Engineering Sciences, University of Colorado Boulder, Boulder, CO, 80309 USA.}\ Hanspeter Schaub\thanks{Glenn L. Murphy Chair of Engineering, Smead Department of Aerospace Engineering 
		Sciences, University of Colorado, 431 UCB, Colorado Center for Astrodynamics Research, Boulder, CO 80309-0431. AAS 
		Fellow.}}

\bibstyle{plain}


\begin{document}
% \nipsfinalcopy is no longer used

\maketitle

% \begin{abstract}
\begin{abstract}
	
	The high cost of space mission operations has motivated several space agencies to prioritize the development of autonomous spacecraft control techniques. ``Learning'' agents present one manner in which autonomous spacecraft can adapt to changing hardware capabilities, environmental parameters, or mission objectives while minimizing dependence on ground intervention. This work considers the frameworks and tools of Deep Reinforcement Learning to address high-level mission planning and decision-making problems for autonomous spacecraft, under the assumption that sub-problems have been addressed through design. Two representative problems reflecting challenges of autonomous orbit insertion and science operations planning, respectively, are presented as Partially-Observable Markov Decision Processes and addressed with Deep Reinforcement Learners to demonstrate the potential benefits of this approach. Sensitivity to initial conditions and learning strategy are discussed and analyzed. Preliminary results indicate substantial improvement over baseline policies in the science-planing case.
\end{abstract}
% \end{abstract}


\section{Introduction}
Spacecraft autonomy has long been regarded as a ``holy grail'' of spacecraft guidance, navigation, and control research\cite{Frost2010}. Decades of development have yielded few fully autonomous spacecraft; instead, mission planners and operators increasingly rely on automated planning tools to inform and support high-level decision making, in part due to the ability of human operators to act under environmental uncertainty, changing or competing mission objectives, and hardware failure. Advances in the field of artificial intelligence due to new machine learning techniques present one avenue in which these problems can be addressed without bringing humans into the loop, while also fulfilling the role of a high-level mission planner. This work aims to extend the state-of-the-art in spacecraft autonomy by applying contemporary machine learning techniques to the high-level mission planning problem.

At present, examples of spacecraft autonomy typically falls into two categories: rule-based autonomy and optimization-based autonomy. Rule-based autonomy treats a spacecraft as a state machine consisting of a set of mode behaviors and defined transitions between modes. This class of algorithms is increasingly commonplace in practice. Pioneered by missions like Deep Impact \cite{Kubitschek2005} launched in 2005, more recent cubesat missions such as the PlanetLabs constellation \cite{Foster2016} transition between operational and health-keeping modes (charging, momentum-exchange device desaturation) autonomously without ground contact. Typically, the design of these autonomous mode sequences is independent from the rest of the spacecraft configuration. Rule-based approaches are attractive from an implementation perspective, as they require little computing power and can be tested to validate their rigid transition criteria. Nevertheless, these rigid rule-sets are ``brittle'' to changes in mission parameters, such as hardware failures or new science objectives. They also require accurate understanding of the mission environment in order to prepare for unwanted behavior. Additionally, rule-based approaches do not readily support the integration of multiple competing mission objectives, and require that those trades be made on the ground with humans in the loop before mission sequences are uploaded.

In contrast to rule-based approaches are a class of tools that use models of spacecraft behavior and hardware to generate mission plans on the ground while considering mission objectives, which this work broadly describes as ``optimization-based'' autonomy. Within this class of algorithms, the spacecraft and its mission are viewed in the framework of constrained optimization, with the spacecraft's hardware and trajectory acting as constraints and metrics of mission return---images taken, communication link uptime, or other criteria---are the values being optimized. In contrast to rule-based autonomy, optimization-based autonomy typically requires large amounts of computing power that precludes their use on-board. This method also requires realistic models, and well developed testing environments. Examples of this work include the Applied Physics Laboratory's SciBox software library (used to generate MESSENGER mode sequences) and the ASPEN mission planning suite developed by the Jet Propulsion Laboratory and applied to the Earth Observing-1 mission \cite{Chien2010}.

As both rule- and optimization-based autonomy techniques become more mature, the search for ``next-generation'' spacecraft autonomy approaches has begun. As with contemporary approaches, emerging techniques in autonomy should reduce mission development and operational cost while improving mission returns. New techniques should also improve upon autonomy runtime, ability to deal with uncertainty, ability to learn from past experiences, and the ability to make mission-level decisions. The need for adaptability strongly suggests the use of machine learning (ML) techniques as a core of autonomous decision software.

A small collection of other works in the application of machine learning techniques to spacecraft problems exists in the recent literature, mostly focusing on the application of learning approaches to low-level control problems in uncertain environments. Several works \cite{Harris2018_a} \cite{Cianciolo2018} have considered reinforcement learning in the context of autonomous aerobraking planners, with mixed results. Others have explored machine learning techniques for asteroid proximity operations \cite{Gaudet2012}. Importantly, these approaches have focused on low-level control with reinforcement learning, an area that has been traditionally covered by conventional estimation and control techniques with great success. In contrast, this work will explicitly examine applications of reinforcement learning to high-level spacecraft planning and decision-making problems that have traditionally been the domain of rigid expert policies or optimization-focused strategies.

Recent advances in machine learning may hold the key to these next-generation approaches for spacecraft autonomy and on-board decision-making. The reinforcement learning approach presented brings these two methods together. A spacecraft is now treated as a state machine and a reward function is defined according to the mission objectives and requirements. Then a learner can optimize the sequence of states that lead to a desired behavior. 

This work explores an approach to fit spacecraft autonomy into the Partially-Observable Markov Decision Process framework and its general solution through model-free ``Deep-Q'' reinforcement learning. First, background on POMDPs and RL are presented and their relevance to the spacecraft autonomy problem are outlined. Next, several demonstration problems representative of applications for spacecraft autonomy are presented within this framework, and solved using Deep-Q reinforcement learning. Finally, these results are discussed and future work within the field outlined. 

\section{Problem Statement}

This work aims to examine the application of Deep Reinforcement Learning techniques to the problem of high-level spacecraft planning, with the intention of developing robustness to hardware failure or changing environmental parameters without humans in-the-loop. Mission-level decision-making for spacecraft occurs in the context of both physical constraints on vehicle resources and multiple competing mission objectives, representing a challenging, multi-faceted optimization problem for prospective planners to handle; the addition of uncertain environmental parameters (such as atmospheric drag or higher-order gravity terms) or on-board failures further complicates this problem. A common framework for representing and ``solving'' such problems is the Partially Observable Markov Decision Process (POMDP). POMDPs compactly represent the processes facing a software agent acting in an evolving environment according to some higher-level objective, and are therefore well-suited to a wide variety of problems. The mathematics of such processes, and challenges associated with them, are reviewed briefly here.

A model of several time-steps of a classical POMDP is presented in Figure \ref{fig:pomdp}, and discussed further here. As in traditional Markov Decision Processes, the state in a POMDP is updated by a transition function $F$, and at any given time can be computed as a function of the previous state and the most recent action taken by the considered agent(s):
\begin{equation}
s_k = F(s_{k-1},a_{k-1})
\end{equation}
This state $s_k$ is observed by the agent according to some observation function $H$:
\begin{equation}
o_k = H(s_k)
\end{equation}
Given an observation $o_k$ of the state, the agent then selects an action $a_k$ to influence the future state according to some policy $\pi$:
\begin{equation}
a_k = \pi(o_k)
\end{equation}
While these transition functions represent physical or software-defined process dynamics, the objective of an agent is ultimately motivated by a reward function $R$:
\begin{equation}
r_k = R(s_{k-1},a_{k-1},s_k)
\end{equation}
The objective of a software agent within a POMDP is to select a policy $\pi$ that maximizes its realized reward. In the most general case, no conditions are placed on any of these functions; while the general POMDP problem is not tractable, considerable success has been had in finding approximate solutions. Importantly, the use of POMDPs allows for the problem of decision-making to be re-framed as an optimization problem, wherein agents attempt to ``optimize'' their behavior according to their knowledge of the environment and an externally-specified reward function. 

While the general POMDP case places no restrictions on the nature of any of the transition functions or states, the consideration of infinite-dimensional, continuous state and action spaces can be extremely computationally intensive. For this reason, many applied autonomy approaches that leverage POMDPs perform some degree of discretization to their state or action space. Additionally, it is noted that POMDPs attempt to describe holistic, system-level problems within a unified framework that is theoretically related to but practically divorced from traditional estimation and control approaches. For these reasons, POMDP-based approaches to autonomy are most frequently studied in cases where traditional estimation and controls approaches are not readily tractable, including human-assisted machine decision-making or multi-vehicle coordination problems. 

\begin{figure}[ht]
	\centering
	\includegraphics[width = 0.4\textwidth]{Diagrams/classicalPOMDP.png}
	\caption{Partially Observable Markov Decision process framework for considering decision problems.}
	\label{fig:pomdp}
\end{figure}

For a spacecraft, the general high-level autonomy POMDP can be stated as follows. Given the constraints of orbital dynamics, on-board hardware, and pre-defined software behaviors, select the sequence of behaviors that best satisfies mission objectives.

\subsection{Reinforcement Learning Solutions}
Astrodynamics and spacecraft-planning problems are typically considered in the context of continuous estimation and control, as many of the processes facing such systems are infinite-dimensional with well-understood, reasonably accurate models. Unfortunately, the high-level relationships between spacecraft actions and the satisfaction of mission objectives is less analytically tractable, and frequently mixes discrete reward states (such as whether a geological feature has been imaged) with continuous ones (such as the management of spacecraft power states). Reinforcement Learning techniques---a subtype of machine learning which focuses on deriving solutions to POMDPs from prior experience---are not restricted to addressing problems with analytical models, or indeed with any models at all. 

Traditionally considered in the context of discretized systems (``tabular'' RL), recent advancements in the training of large Artificial Neural Networks (ANNs) has spurned renewed interest in so-called ``Deep'' RL techniques. In comparison to traditional tabular reinforcement learning, Deep RL can directly consider infinite-dimensional input and output spaces without the need for discretization, and without losing the ability to address systems consisting of both discrete and continuous states and actions. A suite of representative Deep Learners has been implemented in Python for use with OpenAI's \verb|gym| environment, which provides a standard interface for reinforcement learning problems. A portion of this work will explore the performance of different model-free Deep Reinforcement learners with respect to end performance, computational cost, and data efficiency. 

\subsection{Mode-Based Planning}
A long-standing problem in the application of reinforcement learning is the manner in which prior knowledge about a given system can be applied \cite{Sutton2012}. For space applications, many ``low-level'' control and estimation problems are well-solved by traditional techniques; rather than training a learner to re-implement orbital mechanics, it is preferable to leverage existing domain knowledge and shift the application of autonomy algorithms to high-level mission planning. In effect, this approach breaks the spacecraft command problem into a set of sub-problems that are assumed to be addressed using traditional approaches, allowing the autonomous agent to focus on the proper scheduling and sequencing of these software ``modes'' to meet mission objectives.

Mission sequences are discretized in time and dynamics by the application of spacecraft operational ``modes,'' which describe discrete classes of spacecraft states. Modes occur for finite durations reflecting the needs of the behavior compartmentalized by said mode; for example, a ``control'' mode would be designated to run for the settling time of the control system before handing off command of the spacecraft systems back to the planning agent. 

This approach has a number of benefits. First, the specific behaviors of individual modes can be abstracted away and solved using prior knowledge and traditional techniques, allowing for existing knowledge about spacecraft control or mission needs to be applied. At the same time, this approach reduces the action space for a planner from an infinite-dimensional space of control inputs to a set of discrete modes, thereby reducing computational burden. Finally, this action realization does not preclude the use of continuous state, observation, or reward spaces.

\section{Representative Scenarios}
While Deep-Q Networks and Partially-Observable Markov Decision processes are general enough to be applied to virtually any problem, implementation-specific details can be sufficient to prevent learner convergence or harm performance. For this reason, a pair of scenarios reflecting challenges faced by an autonomous spacecraft attempting an interplanetary mission are presented here as POMDPs. Both scenarios share common dynamics models and several common actions, and can therefore be taken to represent the same spacecraft at different points in its mission life-cycle. 

The ``true'' non-linear dynamics resulting from gravity interactions are taken to follow the two-body equations of motion in the presence of perturbing accelerations:
\begin{equation}
\ddot{\bm r} = \frac{-\mu}{r^3} \bm r + \bm a_p
\end{equation}
At the same time, a pre-defined reference trajectory obeying two-body dynamics without perturbing accelerations is used to define the desired mission:
\begin{equation}
\ddot{\bm r}^* = \frac{-\mu}{r^*^3} \bm r^*
\end{equation}
This ignorant propagator is also used to propagate forward the spacecraft's current orbital state estimate, $\hat{x}$. The resulting state and estimate errors are defined as
\begin{equation}
\bm e_s = \bm x - \bm x^*,\ \bm e_\text{est} = \bm x - \hat{\bm x}
\end{equation}
Likewise, the spacecraft-internal control error is defined as:
\begin{equation}
\bm e_c = \hat{\bm x} - \bm x^*
\end{equation}
A team of clever guidance and controls engineers has devised a pair of mutually-exclusive software states that cause the spacecraft's estimation and control errors to decay exponentially. A necessary task for both of the presented scenarios therefore includes the management of estimate and control error while accomplishing other mission objectives. Under the mode-based planning paradigm, the planner considers the state and error dynamics over discrete time-steps that are scenario specific.

These models, alongside a family of deep reinforcement learning agents, was implemented in Python using the OpenAI \verb|gym| framework \cite{OpenAI} to represent the spacecraft-mode POMDP interface in a standardized manner. The deep learning agents were created using Keras \cite{Keras} using Tensorflow \cite{Tensorflow} back-end, providing a 

\subsection{Science Station Keeping}

After orbit insertion, spacecraft typically transfer to a ``mission'' orbit and begin conducting operations. However, while in this orbit, un-modeled perturbations will steady force the spacecraft away from its desired orbit, thereby degrading or preventing mission operations from taking place.

This environment approximates this behavior with the Estimation and Control modes described above with the addition of a reward-producing ``science'' mode that reflects mission-oriented behaviors.

\begin{itemize}
   \item \textbf{Science Mode:} In this mode, the spacecraft conducts valuable science or mission operations and receives a positive reward proportional to how close its true position is to the specified reference position. Neither the estimate nor the true position is controlled; both drift away from the reference. 
\end{itemize}

Ten individual modes are considered, approximating one orbit's worth of behavior.

\subsection{Orbit Insertion}

The first environment simulates insertion into orbit about a planet. This is an example of an interplanetary mission in which a spacecraft is flying towards Mars, and needs to conduct an impulsive maneuver at the correct time to enter orbit about Mars under uncertain knowledge of its state. This type of decision is crucial to the success of many interplanetary missions, and therefore represents an important challenge for proposed autonomy systems. This is also an example of a timely maneuver in which any unexpected behavior could be lethal to the mission as ground teams could not react in time.

Under the mode-control paradigm, the spacecraft considers an additional mode, ``thrust,'' which applies an impulsive $\Delta V$ at a specific time and reflects a major maneuver to adjust both its true and reference trajectory. The challenge of the orbit insertion scenario is therefore to ensure that this thrust is applied at the correct time to ensure orbit insertion, while at the same time maintaining accurate position estimates and controlling towards a defined reference trajectory.

\begin{figure}[ht]
	\centering
	\includegraphics[width = 0.8\textwidth]{moi/ref_test}
	\caption{Reference Trajectory changed by thrust maneuver}
	\label{fig:ref_moi}
\end{figure}

\section{Scenario Results}

At the writing of this abstract, preliminary results have been generated for the aforementioned Science Station-Keeping scenario. Final results will include converged, learned solutions for both the Orbit Insertion and Science Station Keeping environments using a variety of learners and reward structures to analyze performance and convergence, with the intention of informing best-practices for future space-based machine learning approaches.

\subsection{Science Station Keeping}
\label{sec:sk_design}
A Deep-Q learning agent was implemented to address the aforementioned Science Station Keeping environment. Initially, a simple Deep-Q network was implemented to control the system. The network inputs were taken to be the reference position and velocity, the estimated position and velocity, and the estimated position and velocity covariances to represent the spacecraft's knowledge of uncertainty. Combined, this represented an 18-state observation vector at the end of each mode. Reward was provided in a sparse manner based on the actions taken:
\begin{equation}
r_{sci} = r_{base} * \frac{1}{(1 + \bm e_i^T \bm e_i)}
\end{equation}
which ensures that the reward in science mode is always greater than or equal to zero, but still drops off as the orbit error increases. Reward in other modes is zero, reflecting their lack of contribution to the mission objective. Additionally, by ensuring that the reward is always positive, this process simplifies troubleshooting the training of the Q-approximating neural network and avoids large swings in the value of Q depending on whether or not science is performed. 

\begin{figure}[ht]
	\centering
	\includegraphics[width = 0.4\textwidth]{stationKeep/121000_runs.pdf}
	\caption{Reward vs. Episode for StationKeep considering 18-state observation vector. High early rewards are the result of an ``expert'' prior policy that cycled through available actions.}
	\label{fig:trash_station_keep}
\end{figure}
In the hope of improving performance, an ``expert-designed'' prior policy was used to initialize the neural network before engaging in random exploration/exploitation. Unfortunately, this ``naive'' approach to deep-Q learning frequently yielded negative results, as shown by the reward versus training episode plot in Figure \ref{fig:trash_station_keep}.


To remedy this, a simplified observation structure was provided as an input to the neural network. Rather than including all 18 desired, estimated, and covariance states, the learner was provided with the 2-norm of the estimated control error and the 2-norm of the diagonals of the covariance matrix.
\begin{equation}
O = \{\delta x = ||\hat{\bm x} - \bm x^*||, C = ||\text{diag}(C) ||\}
\end{equation}

This has two effects: first, it simplifies the observation from eighteen states to two, which reduces the number of parameters the network needs to train on; second, it directly relates the input parameters to the designated control modes, aiding linear separability (which is thought to improve RL performance)\cite{Henderson2017}. Under this modification, performance of the learner dramatically improved in terms of both training time and received reward. Under the final hyperparameters listed in Table \ref{table:sk_hyper}, as well as the addition of deep eligibility traces to the learner, the positive results of Figure \ref{fig:finalStationKeep} were generated. As shown in Figure \ref{fig:finalStationKeep}, the converged learner improves upon the expert prior policy by a factor of 1.5$\times$, and successfully learns to estimate and control before entering a science mode. 
\begin{figure}[h]
	\centering
	\subfloat[Reward as a function of episode during training.]{
		\includegraphics[width=2.5in]{stationKeep/stationkeepscience_10k_results.pdf}}
    
	\subfloat[Mode sequence produced by the converged algorithm. Lines represent observed covariance (blue), and control error (red). Blue bars indicate an estimator mode, red bars indicate a control mode, and green bars indicate a science mode.]{
		\includegraphics[width = 2.72in]{stationKeep/science_modes_final.pdf}
	}
    \subfloat[Reward over the mode sequence listed in b). Blue bars represent per-mode control, while orange represents the cumulative reward.]{\includegraphics[width=2.5in]{stationKeep/science_rewards_final.pdf}}
	\caption{\label{fig:finalStationKeep} Converged station keeping results}
    
\end{figure}


\begin{table}
  \caption{\label{table:sk_hyper} Hyperparameters used in training the final StationKeep iteration.}
  \centering
    \begin{tabular}{c|c}
    	Parameter & Value/Type \\
        \hline
		Number of Hidden Layers & 1\\ 
        Hidden Layer Depth & 64 \\
        Hidden Layer Activation & ReLU \\
        Output Layer Activation & Linear \\
        Science Reward Multiplier & 10 \\
        Learning Rate & 0.01 \\
        Number of Training Episodes & 10,000 \\
        Annealing Segment Length & 3,000 
    \end{tabular}
\end{table}

\section{Conclusion}
This work has successfully reformulated the general spacecraft decision problem into the Partially-Observable Markov Decision Process framework, setting the stage for future studies in this area through an open-source library using OpenAI's \verb|gym| framework.
The use of reinforcement learning techniques has been adapted to the spacecraft state machine paradigm: combining Deep-Q learning \EditTB{with} POMDP to produce policies that are comparable to ``expert'' priors. 

Various reward structures, hyperparameters, and environment parameters have been considered throughout the experimentation. It appears that failure to obtain positive results in applying Deep-Q reinforcement learning to the spacecraft control problem is the result of insufficient sampling of the problem space. This issue is exacerbated by the large computational requirements for the environments, which slows the training problem considerably compared to simpler environments.

Future work includes the investigation of model-based reinforcement learning techniques to decrease sample requirements and leverage existing knowledge about the space environment. Additionally, higher-speed models built using the \textit{Basilisk} astrodynamics framework will be investigated to speed training time. Furthermore, in order to further improve the MOI performance, it could be judicious to work back up from a functional station keeping environment. By adding the thrust command with high delta-V, and by restricting the authority of the control action, the learner will need to use this action to minimize costs. Adding this and progressively adding complexity the scenario could yield more consistent results.

\medskip
\bibliographystyle{abbrv}  
\bibliography{fucking_references}

\end{document}
