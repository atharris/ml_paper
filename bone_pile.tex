% use paper, or submit
% use 10pt, 11pt or 12pt
% nopagenum removes the page numbers (final conference PDF paper)

%\documentclass[preprint, paper,11pt]{AAS}	% for preprint proceedings
\documentclass[]{aiaa-tc}		% for final proceedings (20-page limit)
%\documentclass[paper,12pt]{AAS}		% for final proceedings (20-page limit)
%\documentclass[paper,10pt]{AAS}		% for final proceedings (20-page limit)
%\documentclass[cover, article,11pt]{AAS}		% for journal paper version
%\documentclass[submit]{AAS}					% to submit to JAS

\usepackage{bm}
\usepackage{amsmath}
%\usepackage[notref,notcite]{showkeys}  % use this to temporarily show labels
\usepackage[colorlinks=true, pdfstartview=FitV, linkcolor= black, citecolor= black, urlcolor= black]{hyperref}
\usepackage{overcite}
\usepackage{AVS}
\usepackage{graphicx}
\usepackage{float}
\usepackage{wrapfig}
 \usepackage{varioref}%  smart page, figure, table, and equation referencing
 \usepackage{wrapfig}%   wrap figures/tables in text (i.e., Di Vinci style)
 %\usepackage{threeparttable}% tables with footnotes
 \usepackage{dcolumn}%   decimal-aligned tabular math columns
 \newcolumntype{d}{D{.}{.}{-1}}
 \usepackage{nomencl}%   nomenclature generation via makeindex
 \makeglossary
 %\usepackage{subfigure}% subcaptions for subfigures
 \usepackage{subfig}
 %\usepackage{subfigmat}% matrices of similar subfigures, aka small mulitples
 \usepackage{fancyvrb}%  extended verbatim environments
 \fvset{fontsize=\footnotesize,xleftmargin=2em}
 \usepackage{lettrine}%  dropped capital letter at beginning of paragraph

\usepackage{color}
\usepackage[normalem]{ulem} % added this to enable sout for strike outs

\definecolor{RED}{rgb}{1,0,0} 
\definecolor{BLUE}{rgb}{0,0,1} 
\definecolor{GREEN}{rgb}{0,0,1} 
%\newcommand{\EditHPS}[1]{{\color{red} #1}}
%\newcommand{\EditHPSd}[1]{{\color{red} \sout{ #1}}}
\newcommand{\EditEH}[1]{{\color{blue} #1}}
\newcommand{\EditEHd}[1]{{\color{blue} \sout{ #1}}}

% Define commands to assure consistent treatment throughout document
\newcommand{\eqnref}[1]{(\ref{#1})}
\newcommand{\class}[1]{\texttt{#1}}
\newcommand{\package}[1]{\texttt{#1}}
\newcommand{\file}[1]{\texttt{#1}}
\newcommand{\BibTeX}{\textsc{Bib}\TeX}

\graphicspath{{./figures/} }
% for a journal simulation cover page:

%\JournalName{Journal of Guidance, Navigation and Control}
%\JournalIssue{Volume~xx, Number~xx, Jan.--Feb., 2001, Pages xx--xx}

% journal article simulation:

%\ArticleIssue{Vol.~24, No.~1, Jan.--Feb., 2001}% first page
%\ArticleHeader{Schaub Et Al: New Penalty Functions}% subsequent pages

% journal note simulation:

%\NoteHeader{J.Guidance, Vol.~20, No.~13: Engineering Notes}

% set copyright and other notices to appear
% as a footnote at the bottom of the first page:

%\PaperNotice{\CopyrightB{1998}{Hanspeter Schaub}}



\title{Markov Decision Framework Approaches for Spacecraft Planning Autonomy}

\author{Andrew Harris\thanks{Research Assistant, Ann and H.J. Smead Department of Aerospace Engineering Sciences, Graduate 
Student, University of Colorado Boulder, Boulder,
		CO, 80309 USA.}\ 
Tibtob Teal,\thanks{Research Assistant, Ann and H.J. Smead Department of Aerospace Engineering Sciences, Graduate Student, University of Colorado Boulder, Boulder, CO, 80309 USA.}\ Hanspeter Schaub,
		}



\begin{document}

\maketitle{} 	

\begin{abstract}
	 Spacecraft autonomy promises to reduce mission costs and enable new mission architectures. This work presents an adaptation of a general Partially-Observable Markov Decision Process for spacecraft decision-making, and demonstrates the utility of this approach by applying contemporary Deep Reinforcement Learning techniques to solve the decision problem. This approach opens the door to the application of modern machine-learning techniques for autonomy to the space field.
	 
\end{abstract}
\section*{Nomenclature}

\begin{tabbing}
	XXX \= \kill% this line sets tab stop%
	$\bm r$ \> Vector \\
	$\bm r_{O/H}$ \> Vector from point $O$ to point $H$\\
	$r$ \> Magnitude of vector $\bm r$ \\
	$ [R] $ \>  Denotes a matrix, $R$\\
	$ \hat{\bm r} $ \> Unit vector \\
	$ \tilde{\bm r}$ \> Skew-symmetric matrix of vector $\bm r$ \\
	$ \leftexp{B}{\bm r}$ \> Vector expressed in $B$ frame\\
	$\bm \dot{r} $ \> Inertial time derivative\\
	$\leftexp{B}{\frac{dr}{dt}} $\> Time derivative as seen by $B$ frame \\
	$ [BN] $ \> Rotation matrix from frame $N$ to frame $B$\\
	$ C_D$ \> Nondimensional drag coefficient \\
	$ C_L$ \> Nondimensional lift coefficient \\
	$ \bm \omega_{B/N} $ \> Spacecraft angular velocity vector between frames $B$ and $N$ \\
	$ \rho$ \> Local neutral atmospheric density \\

	$ A  $ 	\> Area\\[5pt]
	
	\textit{Subscript}\\
	$n$ \> Panel normal vector\\
	$p$ \> Panel position vector\\
	$i$ \> Panel number
\end{tabbing}


\section{Introduction}
Spacecraft autonomy has long been regarded as a ``holy grail'' of spacecraft guidance, navigation, and control research. Despite years of development, examples of autonomous spacecraft systems remain sparse. At the same time, examples of Earth-based autonomous robotic systems are proliferating, in part thanks to advances in machine learning (ML) techniques. This work examines the benefits and architectures of ML-based autonomy techniques and their application to the space-based problem.

At present, examples of spacecraft autonomy typically falls into two categories: ``rule-based'' autonomy,  and optimization-based autonomy. Rule-based autonomy treats a spacecraft as a state machine consisting of a set of mode behaviors and defined transitions between modes. This class of algorithms is increasingly commonplace in practice; in addition to pioneering missions like Deep Impact \cite{kubi1998}, cubesat missions like MarCO \cite{bibid} or the PlanetLabs constellation \cite{bibid} transition between operational and health-keeping modes (charging, momentum-exchange device desaturation) autonomously without ground contact. Typically, the design of these autonomous mode sequences is independent from the rest of the spacecraft configuration. Rule-based approaches are attractive from an implementation perspective, as they require little compute power and can be tested to validate their rigid transition criteria. At the same time, these rigid rule-sets are ``brittle'' to changes in mission plans, such as  hardware failure or new science objectives. Additionally, rule-based approaches do not readily support the integration of multiple competing mission objectives, and require that those trades be made on the ground with humans in the loop before mission sequences are uploaded.

In contrast to rule-based approaches are a class of tools that use models of spacecraft behavior and hardware to generate mission plans on the ground while considering mission objectives, which this work broadly describes as ``optimization-based'' autonomy. In contrast to rule-based autonomy, optimization-based autonomy typically requires large amounts of computing power that precludes their use on-board. Within this class of algorithms, the spacecraft and its mission are viewed in the framework of constrained optimization, with the spacecraft's hardware and trajectory acting as constraints and metrics of mission return--images taken, communication link uptime, or other criteria--are the values being optimized. Examples of this work include the Applied Physics Laboratory's SciBox software library (used to generate MESSENGER mode sequences) and the ASPEN mission planning suite developed by the Jet Propulsion Laboratory and applied to the Earth Observing-1 mission.

As both rule- and optimization-based autonomy techniques become more mature, the search for ``next-generation'' spacecraft autonomy approaches has begun. As with contemporary approaches, emerging techniques in autonomy should reduce mission development and operational cost while improving mission returns. New techniques should also improve upon autonomy runtime, ability to deal with uncertainty, ability to learn from past experiences, and the ability to make mission-level decisions. 

Recent advances in machine learning may hold the key to these next-generation approaches for spacecraft autonomy and on-board decision-making. ML approaches 

This work explores an approach to fit spacecraft autonomy into the Partially-Observable Markov Decision Process framework and its general solution through model-free ``Deep-Q'' reinforcement learning. First, background on POMDPs and RL are presented and their relevance to the spacecraft autonomy problem outlined. Next, several demonstration problems representative of applications for spacecraft autonomy are presented within this framework, and solved using Deep-Q reinforcement learning. Finally, these results are discussed and future work within the field outlined. 

\section{Machine Decision-Making Primer}
\subsection{Decision-Making Frameworks}
Since their inception, Markov Decision Processes have become a de-facto standard in autonomous decision research. 

\section{Reinforcement Learning}

\section{Simulated Control Problems}
\subsection{Science Operations}

\subsection{Orbit Insertion}


\section{Conclusions and Future Work}


\section{Acknowledgements}
This work was supported by the Department of Defense National Defense Science and Engineering Graduate Fellowship (NDSEG) Program, the Air Force Scholars Program, and the CU Smead Scholars Program.
\bibliographystyle{aiaa}   % Number the 
\bibliography{aeroRefs}   % Use references.bib to resolve the labels.



\end{document}
